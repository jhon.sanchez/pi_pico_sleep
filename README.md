# LTT01 FPButton - V1

## INSTALL Dialog SmartSnippets Toolbox
To install correctly the Smartbond flash programmer by Renesas, Go to this <a href="https://www.renesas.com/tw/en/products/interface-connectivity/wireless-communications/bluetooth-low-energy/da14531-smartbond-ultra-low-power-bluetooth-51-system-chip#design_development">**link**</a> and install the the Smartbond flash programmer according to your OS.

<img src="Photos/Smart_Install.png" width="750" style="vertical-align:middle" style="float:right">

## Hardware Connection

### USB Dev Kit Configuration
Set the **USB Dev Kit** as indicatd in the image in order to flash the DA14531

<img src="Photos/Hw_Connection.png" width="750" style="vertical-align:middle" style="float:right">

### Pin Connection 

Connect the pins in the following way
* Connect **SCL** pin to SWCLK(C in LTT01)
* Connect **RST** pin to SWDIO(D in LTT01)
* Connect **3V3** pin to VDD(V in LTT01)
* Connect **GND** pin to GND(G in LTT01)

<img src="Photos/Hw_Connection_2.png" width="750" style="vertical-align:middle" style="float:right">


## Flashin the DA

Once the above steps have been completed open the **Dialog Smartbond Flash Programmer** and follow the steps

* 1. Check the JTAG detected and select it
* 2. Click **Erase** button and wait until "**Memory erasing completed successfully**"

<img src="Photos/Smart_Install_2.png" width="1000" style="vertical-align:middle" style="float:right">


### The last FW version can be found  <a href="https://gitlab.com/titoma-ltd/ltt01/ltt01-fw-da14531-mother-v1/-/releases">**here**</a>

* 3. click on **Browse** and proceed to import 
* 4. select the last FW version (**BOOTLOADER** NOT *OTA* )
* 5. click **Open** Button
* 6. Click **Program** Button

<img src="Photos/Smart_Install_4.png" width="1000" style="vertical-align:middle" style="float:right">


* 7. Once the device start Flashing, Wait for the *"Starting Application on chip. If application does not start automatically, press the RESET button"* message.
* 8. Whether some error shows up on message log window, repeat the flashing process and if the error persists it might be a hardware problem.

<img src="Photos/Smart_Install_5.png" width="1000" 
style="vertical-align:middle" style="float:right">

* Disconnect the progamming cables and USB Dev Kit